## FireBase
Firebase Analytics is built from the ground up to provide all the data that mobile app developers need, in one easy place. 
It's free and unlimited analytics that combines both behavioural analytics -- who your users are and what they do in your app -- with marketing analytics -- which lets you see which campaigns brought users to your app in the first place.

###Why Your Next App Should Be FireBase Base
	Comprehensive Analytics
•	Login Analysis/ Easy Auth 
•	Easily Categorize your users (big fish, music type, favourite app etc)

	Easy Messaging to User
•	You can easily group your users and serve them with appropriate ads or messages 
•	Very easy and fast when firing the notifications( sent within 250ms with 95% delivery success)
	Test Lab
•	Easily/Automatically  test your app across all devices using Firebase ROBO, 
•	No coding just upload your app  

	Crash Reporting
•	Easily report your app crashes in a better and graphical manner for easy analysis
•	User category, device Architecture / config, and during what activity 

###Demo (Using Android Studio 2.2)

1.	####Necessary tools
..*Android device or emulator with at lest Gingerbread (api 9)
..*Google Play services (9.0 or higher _ demo this _**)
..*Android SDK tool (google play services 30.0 and google Repository 28.0 atlest _ demo this _**)
2.	####Working with firebase console
..*Demo this
..*Sha 1 command >
..*keytool -list -v -keystore C:\Users\nokot\.android\debug.keystore -alias androiddebugkey -storepass android -keypass android
..*fire base core[https://goo.gl/nKwN8I]( https://goo.gl/nKwN8I)
 
###Back to studio 

